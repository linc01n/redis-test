package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/go-redis/redis"
	_ "github.com/mattn/go-sqlite3"
)

func main() {
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	client.FlushAll()

	startT := time.Now()
	for i := 0; i < 1000000; i++ {
		writeToRedis(client, i)
	}
	endT := time.Now()

	fmt.Printf("Command Cost: %s\n", endT.Sub(startT))

	client.FlushAll()

	startT = time.Now()
	pipe := client.Pipeline()

	for i := 0; i < 1000000; i++ {
		pipe.HSet(fmt.Sprintf("car%d", i), "price", i)
	}

	pipe.Exec()
	endT = time.Now()
	fmt.Printf("Pipeline Cost: %s\n", endT.Sub(startT))

	startT = time.Now()
	sqlite3Insert()
	endT = time.Now()
	fmt.Printf("Sqlite Cost: %s\n", endT.Sub(startT))
}

func writeToRedis(client *redis.Client, num int) {
	client.HSet(fmt.Sprintf("car%d", num), "price", num)
}

func sqlite3Insert() {
	os.Remove("./dump.sqlite3")

	db, err := sql.Open("sqlite3", "./dump.sqlite3")

	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	sqlStmt := `
        CREATE TABLE person (id integer not null primary key, name text);
        DELETE from person;
    `

	_, err = db.Exec(sqlStmt)

	if err != nil {
		log.Printf("%q: %s\n", err, sqlStmt)
		return
	}

	tx, err := db.Begin()
	if err != nil {
		log.Fatal(err)
	}

	stmt, err := tx.Prepare("insert into person(id, name) values(?, ?)")
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()

	for i := 0; i < 1000000; i++ {
		_, err = stmt.Exec(i, fmt.Sprintf("Anju%03d", i))
		if err != nil {
			log.Fatal(err)
		}
	}
	tx.Commit()

}
